<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Modelo de Articulos
 */
class ArticulosModel extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function obtenerArticulos()
	{
		/**
		 * funcion apra ahcer la consulta que trae todas los articulos
		 */

		$query = "SELECT art.id, art.clave, art.descripcion, art.encargado, DATE_FORMAT(art.fechaAlta, '%d-%m-%Y') as fechaAlta, dir.nombre as direccion
				FROM articulos as art
				INNER JOIN direcciones as dir ON art.id_direccion = dir.id";

		return $this->db->query($query)->result();
	}

	public function agregarArticulo($data)
	{
		/**
		 * funcion guardar un nuevo articulo
		 */

		$this->db->trans_begin();
		$this->db->query("INSERT INTO articulos (clave, descripcion, encargado, fechaAlta, id_direccion) VALUES ('".$data['clave']."','".$data['desc']."','".$data['encargado']."','".$data['fecha']."', ".$data['dir'].")");

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return false;
		}
		else
		{
			$this->db->trans_commit();
			return true;
		}
	}

	public function obtenerArticulosID($ID)
	{
		/**
		 * funcin apra obtener un artículo por su ID
		 */

		$query = "SELECT id, clave, descripcion, encargado, fechaAlta, id_direccion FROM articulos WHERE id = ".$ID;

		return $this->db->query($query)->result();

	}

	public function editarArticulo($data)
	{
		/**
		 * Editaremos el articulo
		 */

		$this->db->trans_begin();
		$this->db->query("UPDATE articulos SET clave = '".$data['clave']."', descripcion = '".$data['desc']."', encargado = '".$data['encargado']."', id_direccion = ".$data['dir']." WHERE id = ".$data['ID']);

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return false;
		}
		else
		{
			$this->db->trans_commit();
			return true;
		}
	}

	public function borrarArticulo($ID)
	{
		/**
		 * Borrar un articulo usando su ID
		 */

		$this->db->trans_begin();
		$this->db->query("DELETE FROM articulos WHERE id = ".$ID);

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return false;
		}
		else
		{
			$this->db->trans_commit();
			return true;
		}
	}

	/********************************************************************************************************************/

	public function obtenerDirecciones()
	{
		/**
		 * obtener las direcciones para cargar el dropdown
		 */

		$query = "SELECT id, nombre, ejercicio FROM direcciones";

		return $this->db->query($query)->result();
	}
}
?>