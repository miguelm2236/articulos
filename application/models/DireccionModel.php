<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Modelo para las direcciones
 */
class DireccionModel extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function obtenerDirecciones()
	{
		/**
		 * funcion apra ahcer la consulta que trae todas las direcciones
		 */

		$query = "SELECT id, nombre, ejercicio, CASE activa WHEN 1 THEN 'Activo' WHEN 0 THEN 'Inactivo' ELSE 'Otro' END AS activo FROM direcciones";

		return $this->db->query($query)->result();
	}

	public function agregarDireccion($data)
	{
		/**
		 * funcion guardar los datos obtenidos
		 */

		$this->db->trans_begin();
		$this->db->query("INSERT INTO direcciones (nombre, ejercicio, activa) VALUES ('".$data['nombre']."','".$data['ejercicio']."',1)");

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return false;
		}
		else
		{
			$this->db->trans_commit();
			return true;
		}
	}

	public function obtenerDireccionID($data)
	{
		/**
		 * Obtener datos de uan direccion pro su ID
		 */

		$query = "SELECT id, nombre, ejercicio FROM direcciones WHERE id = ".$data['ID'];

		return $this->db->query($query)->result();
	}

	public function editarDireccion($data)
	{
		/**
		 * FUncion para editar una direccion
		 */

		$this->db->trans_begin();
		$this->db->query("UPDATE direcciones SET nombre = '".$data['nombre']."', ejercicio = '".$data['ejercicio']."' WHERE id = ".$data['ID']);

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return false;
		}
		else
		{
			$this->db->trans_commit();
			return true;
		}
	}

	public function eliminarDireccion($ID)
	{
		/**
		 * Eliminar uan dirección usando su ID
		 */

		$this->db->trans_begin();
		$this->db->query("DELETE FROM direcciones WHERE id = ".$ID);

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return false;
		}
		else
		{
			$this->db->trans_commit();
			return true;
		}
	}

	public function activarDir($ID)
	{
		/**
		 * activar uan dirección usando su ID
		 */
		$this->db->trans_begin();
		$this->db->query("UPDATE direcciones SET activa = 1 WHERE id = ".$ID);

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return false;
		}
		else
		{
			$this->db->trans_commit();
			return true;
		}
	}

	public function desactivarDir($ID)
	{
		/**
		 * Desactivar uan dirección usando su ID
		 */
		$this->db->trans_begin();
		$this->db->query("UPDATE direcciones SET activa = 0 WHERE id = ".$ID);

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return false;
		}
		else
		{
			$this->db->trans_commit();
			return true;
		}
	}
}
?>