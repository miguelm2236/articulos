<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controlador de articulos
 */
class ArticulosController extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('ArticulosModel');
	}

	public function principal()
	{
		/**
		 * vista principal de articulos
		 */

		$datos['direccion'] = $this->ArticulosModel->obtenerDirecciones();
		$this->load->view("articulos", $datos);
	}

	public function obtenerArticulos()
	{
		/**
		 * Llamar los articulos
		 */
		$ress = $this->ArticulosModel->obtenerArticulos();
		print_r(json_encode($ress));
	}

	public function agregarArticulo()
	{
		/**
		 * funcion guardar un nuevo articulo
		 */

		$data['clave'] = $this->input->post('clave');
		$data['desc'] = $this->input->post('desc');
		$data['encargado'] = $this->input->post('encargado');
		$data['dir'] = $this->input->post('dir');
		$data['fecha'] = date('Y-m-d');

		$ress = $this->ArticulosModel->agregarArticulo($data);
		print_r(json_encode($ress));
	}

	public function obtenerArticulosID()
	{
		/**
		 * Función para obtener un artículo enviando su ID
		 */

		$ID = $this->input->post('ID');
		$ress = $this->ArticulosModel->obtenerArticulosID($ID);
		print_r(json_encode($ress));
	}

	public function editarArticulo()
	{
		/**
		 * Editaremos el articulo
		 */

		$data['ID'] = $this->input->post('ID');
		$data['clave'] = $this->input->post('clave');
		$data['desc'] = $this->input->post('desc');
		$data['encargado'] = $this->input->post('encargado');
		$data['dir'] = $this->input->post('dir');

		$ress = $this->ArticulosModel->editarArticulo($data);
		print_r(json_encode($ress));
	}

	public function borrarArticulo()
	{
		/**
		 * Borrar un articulo usando su ID
		 */

		$ID = $this->input->post('ID');
		$ress = $this->ArticulosModel->borrarArticulo($ID);
		print_r(json_encode($ress));
	}

	/********************************************************************************************************************/

	public function obtenerDirecciones()
	{
		/**
		 * obtener las direcciones para cargar el dropdown
		 */

		$ress = $this->ArticulosModel->obtenerDirecciones();
		print_r(json_encode($ress));
	}
}
?>