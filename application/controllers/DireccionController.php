<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controlador de direcciones
 */
class DireccionController extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('DireccionModel');
	}

	public function index()
	{
		$this->load->view("direcciones");
	}

	public function obtenerDirecciones()
	{
		/**
		 * Funcion para llamar al modelo para obtener direcciones y enviarla a la vista en formato json
		 */
		$ress = $this->DireccionModel->obtenerDirecciones();
		print_r(json_encode($ress));

	}

	public function agregarDireccion()
	{
		/**
		 * funcion apra enviar lso datos obtenidos al modelo y poder guardarlos
		 */

		$data['nombre'] = $this->input->post('nombre');
		$data['ejercicio'] = $this->input->post('ejercicio');
		$resultado = $this->DireccionModel->agregarDireccion($data);
		print_r(json_encode($resultado));
	}

	public function obtenerDireccionID()
	{
		/**
		 * Obtener datos de uan direccion pro su ID
		 */

		$data['ID'] = $this->input->post('ID');
		$resultado = $this->DireccionModel->obtenerDireccionID($data);
		print_r(json_encode($resultado));

	}

	public function editarDireccion()
	{
		/**
		 * funcion donde recibimso los datos a enviar al modelo
		 * 
		 */

		$data['ID'] = $this->input->post('ID');
		$data['nombre'] = $this->input->post('nombre');
		$data['ejercicio'] = $this->input->post('ejercicio');
		$resultado = $this->DireccionModel->editarDireccion($data);
		print_r(json_encode($resultado));
	}

	public function eliminarDireccion()
	{
		/**
		 * obtiene los datos para enviarlos al modelo y eliminar el registro
		 */
		
		$ID = $this->input->post('ID');
		$resultado = $this->DireccionModel->eliminarDireccion($ID);
		print_r(json_encode($resultado));
	}

	public function activarDir()
	{
		/**
		 * obtiene los datos para enviarlos al modelo y activamos la direccion
		 */

		$ID = $this->input->post('ID');
		$resultado = $this->DireccionModel->activarDir($ID);
		print_r(json_encode($resultado));
	}

	public function desactivarDir()
	{
		/**
		 * obtiene los datos para enviarlos al modelo y desactivamos la direccion
		 */

		$ID = $this->input->post('ID');
		$resultado = $this->DireccionModel->desactivarDir($ID);
		print_r(json_encode($resultado));
	}
}
?>