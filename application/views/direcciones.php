<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.min.css">
	<script src="<?php echo base_url();?>assets/js/jquery-3.3.1.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	<title>Direcciones</title>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
	            <ol class="breadcrumb">
	                <li><a href="#">Direcciones</a></li>
	            </ol>
	        </div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
                <div class="row">
                    <div class="container-fluid">
                    	<div class="col-md-2"></div>
                    	<div class="col-md-8">
                    		<div class="card">
	                            <div class="btn-group" data-toggle="buttons" />
	                                <button type="button" title="Nueva direccion" data-toggle="modal" data-target="#modalAgregar" class="btn btn-primary" id="add"><i class="fa fa-plus"></i></button>
	                            </div>
	                            <div class="btn-group"/>
	                                <a href="<?php echo base_url('index.php/ArticulosController/principal');?>" title="Ir a artículos" class="btn btn-default">Articulos</a>
	                            </div>
	                        </div>
	                        <div class="card">
	                            <div class="header text-center">
	                                <legend><strong>Catálogo de Direcciones</strong></legend>
	                            </div>
	                            <div class="content" id="DatosTabla">
	                                <table class="table table-hover table-condensed" id="tblDir" style="text-align: center;">
	                                    <thead>
	                                        <tr>
	                                            <th style="text-align: center;">Nombre</th>
	                                            <th style="text-align: center;">Ejercicio</th>
	                                            <th style="text-align: center;">Activo</th>
	                                            <th style="text-align: center;">Acciones</th>
	                                        </tr>
	                                    </thead>
	                                    <tbody id="buildDataDir">
	                                    	<!-- aquí se contruye la tabla -->
	                                    </tbody>
	                                </table>
	                            </div>
	                        </div>
                    	</div>
                    	<div class="col-md-2"></div>
                        
                    </div>
                </div>  
            </div>
		</div>
	</div>

	<!-- Modal para agregar nueva direccion -->
	<div class="modal fade" id="modalAgregar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  	<div class="modal-dialog" role="document">
		    <div class="modal-content">
				<div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="myModalLabel">Agregar Dirección</h4>
		    	</div>
			    <div class="modal-body">
			        <div class="content">
			            <table width="100%">
			                <tr>
			                    <td style="padding: 10px;">Nombre</td>
			                    <td style="padding: 10px;">Ejercicio</td>
			                </tr>
			                <tr>
			                    <td style="padding: 10px;"><input type="text" class="form-control" id="inputNombre"></td>
			                    <td style="padding: 10px;"><input type="text" class="form-control" id="inputejercicio"></td>
			                </tr>
			            </table>
			        </div>
			    </div>
			    <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			        <button type="button" class="btn btn-primary" id="guardarDir" data-dismiss="modal" onclick="agregarDireccion()">Guardar</button>
			    </div>
		    </div>
		</div>
	</div>

	<!-- Modal para agregar nueva direccion -->
	<div class="modal fade" id="modalEditar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  	<div class="modal-dialog" role="document">
		    <div class="modal-content">
				<div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="myModalLabel">Editar Dirección</h4>
		    	</div>
			    <div class="modal-body">
			        <div class="content">
			            <table width="100%">
			                <tr>
			                    <td style="padding: 10px;">Nombre</td>
			                    <td style="padding: 10px;">Ejercicio</td>
			                </tr>
			                <tr>
			                    <td style="padding: 10px;"><input type="text" class="form-control" id="editNombre"></td>
			                    <td style="padding: 10px;"><input type="text" class="form-control" id="editEjercicio"></td>
			                    <input type="hidden" id="editID">
			                </tr>
			            </table>
			        </div>
			    </div>
			    <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			        <button type="button" class="btn btn-primary" onclick="editarDireccion()" data-dismiss="modal">Editar</button>
			    </div>
		    </div>
		</div>
	</div>
</body>
</html>
<script>
$(document).ready(function(){
	obtenerDirecciones();
});

function obtenerDirecciones()
{
	/**
	 * Funcion para obtener las direcciones y construir el cuerpo de la tabla
	 */

	 $("#tblDir tbody tr").remove();

	 $.ajax({
	 	url:  <?php  echo "'". base_url(). "index.php/DireccionController/obtenerDirecciones" ."'" ?>,
        cache: false,
        async: true,
        dataType: 'html',
        beforeSend: function(html){
        },
        success: function(html){
        	var rows = "";
        	$(jQuery.parseJSON(html)).each(function(){
        		rows = rows + '<tr><td>'+this.nombre+'</td><td>'+this.ejercicio+'</td><td>'+this.activo+'</td><td><button class="btn btn-primary" title="Editar" onclick="obtenerDireccionID('+this.id+')"><i class="fa fa-pencil"></i></button>';
        			if (this.activo == "Activo") 
        			{
        				rows = rows + '<button class="btn btn-danger" data-toggle="tooltip" title="desactivar" onclick="desactivarDir('+this.id+')"><i class="fa fa-window-close"></i></button>';
        			}
        			else
        			{
        				rows = rows + '<button class="btn btn-success" data-toggle="tooltip" title="Activar" onclick="activarDir('+this.id+')"><i class="fa fa-check"></i></button>';
        			}

        			rows = rows + '<button class="btn btn-danger" title="Eliminar" onclick="eliminarDireccion('+this.id+')"><i class="fa fa-trash-o"></i></button></div></td></tr>';
        	});
        	$("#buildDataDir").append(rows);
        }
	 });
}

function agregarDireccion()
{
	/**
	 * Funcion para mandar los datos de los campos a la funcion en el controlador
	 */

	var nombre = $("#inputNombre").val();
	var ejercicio = $("#inputejercicio").val();

	$.ajax({
	 	type: 'POST',
	 	url:  <?php  echo "'". base_url(). "index.php/DireccionController/agregarDireccion" ."'" ?>,
        cache: false,
        async: true,
        dataType: 'html',
        data: 'nombre='+nombre+'&ejercicio='+ejercicio,
        beforeSend: function(html){
        },
        success: function(html){
        	switch(html)
            {
                case 'true':
                	obtenerDirecciones();
                break;
                         
                case 'false':
                	alert("Error al agregar un registro");
                	obtenerDirecciones();
                break;
            }
            limpiarCampos();
        }
    });
}

function obtenerDireccionID(ID)
{
	/**
	 * Obtener una direccion por su ID
	 */
	$.ajax({
        type: 'POST',
        url:  <?php  echo "'". base_url(). "index.php/DireccionController/obtenerDireccionID" ."'" ?>,
        cache: false,
        async: true,
        dataType: 'html',
        data: 'ID='+ID,
        beforeSend: function(html){
        },
        success: function(html){
            var pro = $(jQuery.parseJSON(html));
            $("#modalEditar").modal('show');
            $("#editID").val(pro[0].id);
            $("#editNombre").val(pro[0].nombre);
            $("#editEjercicio").val(pro[0].ejercicio);
        }
    });
}

function editarDireccion()
{
	/**
	 * editarmos una direccion
	 */

	var nombre = $("#editNombre").val();
	var ejercicio = $("#editEjercicio").val();
	var ID = $("#editID").val()

	$.ajax({
	 	type: 'POST',
	 	url:  <?php  echo "'". base_url(). "index.php/DireccionController/editarDireccion" ."'" ?>,
        cache: false,
        async: true,
        dataType: 'html',
        data: 'nombre='+nombre+'&ejercicio='+ejercicio+'&ID='+ID,
        beforeSend: function(html){
        },
        success: function(html){
        	switch(html)
            {
                case 'true':
                	obtenerDirecciones();
                break;
                         
                case 'false':
                	alert("Error al editar un registro");
                	obtenerDirecciones();
                break;
            }
        }
    });
}

function eliminarDireccion(ID)
{
	/**
	 * Elimianr una direccion por si ID
	 */

	$.ajax({
	 	type: 'POST',
	 	url:  <?php  echo "'". base_url(). "index.php/DireccionController/eliminarDireccion" ."'" ?>,
        cache: false,
        async: true,
        dataType: 'html',
        data: 'ID='+ID,
        beforeSend: function(html){
        },
        success: function(html){
        	switch(html)
            {
                case 'true':
                	obtenerDirecciones();
                break;
                         
                case 'false':
                	alert("Error al eliminar un registro");
                	obtenerDirecciones();
                break;
            }
        }
    });
}

function activarDir(ID)
{
	/**
	 * activar una direccion por si ID
	 */

	$.ajax({
	 	type: 'POST',
	 	url:  <?php  echo "'". base_url(). "index.php/DireccionController/activarDir" ."'" ?>,
        cache: false,
        async: true,
        dataType: 'html',
        data: 'ID='+ID,
        beforeSend: function(html){
        },
        success: function(html){
        	switch(html)
            {
                case 'true':
                	obtenerDirecciones();
                break;
                         
                case 'false':
                	alert("Error al activar un registro");
                	obtenerDirecciones();
                break;
            }
        }
    });
}

function desactivarDir(ID)
{
	/**
	 * desactivar una direccion por si ID
	 */

	$.ajax({
	 	type: 'POST',
	 	url:  <?php  echo "'". base_url(). "index.php/DireccionController/desactivarDir" ."'" ?>,
        cache: false,
        async: true,
        dataType: 'html',
        data: 'ID='+ID,
        beforeSend: function(html){
        },
        success: function(html){
        	switch(html)
            {
                case 'true':
                	obtenerDirecciones();
                break;
                         
                case 'false':
                	alert("Error al desactivar un registro");
                	obtenerDirecciones();
                break;
            }
        }
    });
}

function limpiarCampos()
{
	/**
	 * Función apra limpiar los campos del modal de captura
	 */

	var nombre = $("#inputNombre").val("");
	var ejercicio = $("#inputejercicio").val("");
}
</script>