<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.min.css">
	<script src="<?php echo base_url();?>assets/js/jquery-3.3.1.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	<title>Articulos</title>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
	            <ol class="breadcrumb">
	                <li><a href="#">Artículos</a></li>
	            </ol>
	        </div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
                <div class="row">
                    <div class="container-fluid">
                    	<div class="card">
	                        <div class="btn-group" data-toggle="buttons" />
	                                <button type="button" title="Nuevo articulo" data-toggle="modal" data-target="#modalAgregar" class="btn btn-primary" onclick="obtenerDirecciones()" id="add"><i class="fa fa-plus"></i></button>
	                        </div>
	                        <div class="btn-group" />
	                            <a href="<?php echo base_url('index.php/DireccionController/index');?>" title="Ir a direcciones" class="btn btn-default">Direcciones</a>
	                        </div>
	                    </div>
                        <br>
                        <div class="card">
	                        <div class="header text-center">
	                            <legend><strong>Catálogo de Artículo</strong></legend>
	                        </div>
	                        <div class="content" id="DatosTabla">
	                            <table class="table table-hover table-condensed" id="tblArt" style="text-align: center;">
	                                <thead>
	                                    <tr>
	                                        <th style="text-align: center;">Clave</th>
	                                        <th style="text-align: center;">Descripción</th>
	                                        <th style="text-align: center;">Encargado</th>
	                                        <th style="text-align: center;">Fecha Alta</th>
	                                        <th style="text-align: center;">Dirección</th>
	                                        <th style="text-align: center;">Acciones</th>
	                                    </tr>
	                                </thead>
	                                <tbody id="buildDataArt">
	                                    	<!-- aquí se contruye la tabla -->
	                                </tbody>
	                            </table>
	                        </div>
	                    </div>
                    </div>
                </div>  
            </div>
		</div>
	</div>

	<!-- Modal para agregar nueva direccion -->
	<div class="modal fade" id="modalAgregar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  	<div class="modal-dialog" role="document">
		    <div class="modal-content">
				<div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="myModalLabel">Agregar Artículo</h4>
		    	</div>
			    <div class="modal-body">
			        <div class="content">
			            <table width="100%">
			                <tr>
			                    <td style="padding: 10px;">Clave</td>
			                    <td style="padding: 10px;">Descripción</td>
			                </tr>
			                <tr>
			                    <td style="padding: 10px;"><input type="text" class="form-control" id="inputClave"></td>
			                    <td style="padding: 10px;"><input type="text" class="form-control" id="inputDesc"></td>
			                </tr>
			                <tr>
			                    <td style="padding: 10px;">Encargado</td>
			                    <td style="padding: 10px;">Dirección</td>
			                </tr>
			                <tr>
			                    <td style="padding: 10px;"><input type="text" class="form-control" id="inputEnca"></td>
			                    <td style="padding: 10px;"><select class="form-control" name="" id="cmbDir"></select></td>
			                </tr>
			            </table>
			        </div>
			    </div>
			    <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			        <button type="button" class="btn btn-primary" id="guardarArt" data-dismiss="modal" onclick="agregarArticulo()">Guardar</button>
			    </div>
		    </div>
		</div>
	</div>

	<!-- Modal para editar direccion -->
	<div class="modal fade" id="modalEditar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  	<div class="modal-dialog" role="document">
		    <div class="modal-content">
				<div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="myModalLabel">Editar Artículo</h4>
		    	</div>
			    <div class="modal-body">
			        <div class="content">
			            <table width="100%">
			                <tr>
			                    <td style="padding: 10px;">Clave</td>
			                    <td style="padding: 10px;">Descripción</td>
			                </tr>
			                <tr>
			                    <td style="padding: 10px;"><input type="text" class="form-control" id="editClave"></td>
			                    <td style="padding: 10px;"><input type="text" class="form-control" id="editDesc"></td>
			                </tr>
			                <tr>
			                    <td style="padding: 10px;">Encargado</td>
			                    <td style="padding: 10px;">Dirección</td>
			                </tr>
			                <tr>
			                    <td style="padding: 10px;"><input type="text" class="form-control" id="editEnca"></td>
			                    <td style="padding: 10px;"><select class="form-control" name="" id="editCmbDir">
			                    	<?php foreach ($direccion as $dir): ?>
			                    		<?php echo '<option value="'.$dir->id.'">'.$dir->nombre.'</option>' ?>
			                    	<?php endforeach ?>
			                    </select></td>
			                    <input type="hidden" id="editID">
			                </tr>
			            </table>
			        </div>
			    </div>
			    <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			        <button type="button" class="btn btn-primary" id="editarArt" data-dismiss="modal" onclick="editarArticulo()">Editar</button>
			    </div>
		    </div>
		</div>
	</div>
</body>
</html>
<script>
$(document).ready(function(){
	obtenerArticulos();
});


function obtenerArticulos()
{
	/**
	 * funcion apra ahcer la consulta que trae todas los articulos
	 */

	$("#tblArt tbody tr").remove();

	$.ajax({
	 	url:  <?php  echo "'". base_url(). "index.php/ArticulosController/obtenerArticulos" ."'" ?>,
        cache: false,
        async: true,
        dataType: 'html',
        beforeSend: function(html){
        },
        success: function(html){
        	var rows = "";
        	$(jQuery.parseJSON(html)).each(function(){
        		rows = rows + '<tr><td>'+this.clave+'</td><td>'+this.descripcion+'</td><td>'+this.encargado+'</td><td>'+this.fechaAlta+'</td><td>'+this.direccion+'</td><td><button class="btn btn-primary" data-toggle="tooltip" title="Editar" onclick="obtenerArticulosID('+this.id+')"><i class="fa fa-pencil"></i></button><button class="btn btn-danger" data-toggle="tooltip" title="Eliminar" onclick="borrarArticulo('+this.id+')"><i class="fa fa-trash-o"></i></button></td></tr>';
        	});
        	$("#buildDataArt").append(rows);
        }
    });
}

function agregarArticulo()
{
	/**
	 * funcion guardar un nuevo articulo
	 */

	var clave = $("#inputClave").val();
	var desc = $("#inputDesc").val();
	var encargado = $("#inputEnca").val();
	var dir = $("#cmbDir option:selected").val();

	$.ajax({
	 	type: 'POST',
	 	url:  <?php  echo "'". base_url(). "index.php/ArticulosController/agregarArticulo" ."'" ?>,
        cache: false,
        async: true,
        dataType: 'html',
        data: 'clave='+clave+'&desc='+desc+'&encargado='+encargado+'&dir='+dir,
        beforeSend: function(html){
        },
        success: function(html){
        	
        	switch(html)
            {
                case 'true':
                	obtenerArticulos();
                break;
                         
                case 'false':
                	alert("Error al agregar un artículo");
                	obtenerArticulos();
                break;
            }
            limpiarCampos();
        }
    });
}

function obtenerArticulosID(ID)
{
	/**
	 * Función para obtener un artículo enviando su ID
	 */

	$.ajax({
        type: 'POST',
        url:  <?php  echo "'". base_url(). "index.php/ArticulosController/obtenerArticulosID" ."'" ?>,
        cache: false,
        async: true,
        dataType: 'html',
        data: 'ID='+ID,
        beforeSend: function(html){
        },
        success: function(html){
            var pro = $(jQuery.parseJSON(html));
            $("#modalEditar").modal('show');
            $("#editID").val(pro[0].id);
            $("#editClave").val(pro[0].clave);
			$("#editDesc").val(pro[0].descripcion);
			$("#editEnca").val(pro[0].encargado);
			$("#editCmbDir option").filter(function(){
				return $(this).val() == pro[0].id_direccion;
			}).prop("selected", true);
        }
    });
}

function editarArticulo()
{
	/**
	 * Editaremos el articulo
	 */

	var ID = $("#editID").val();
	var clave = $("#editClave").val();
	var desc = $("#editDesc").val();
	var encargado = $("#editEnca").val();
	var dir = $("#editCmbDir option:selected").val();

	$.ajax({
	 	type: 'POST',
	 	url:  <?php  echo "'". base_url(). "index.php/ArticulosController/editarArticulo" ."'" ?>,
        cache: false,
        async: true,
        dataType: 'html',
        data: 'ID='+ID+'&clave='+clave+'&desc='+desc+'&encargado='+encargado+'&dir='+dir,
        beforeSend: function(html){
        },
        success: function(html){
        	
        	switch(html)
            {
                case 'true':
                	obtenerArticulos();
                break;
                         
                case 'false':
                	alert("Error al Editar un artículo");
                break;
            }
        }
    });
}

function borrarArticulo(ID)
{
	/**
	 * Borrar un articulo usando su ID
	 */

	$.ajax({
	 	type: 'POST',
	 	url:  <?php  echo "'". base_url(). "index.php/ArticulosController/borrarArticulo" ."'" ?>,
        cache: false,
        async: true,
        dataType: 'html',
        data: 'ID='+ID,
        beforeSend: function(html){
        },
        success: function(html){
        	switch(html)
            {
                case 'true':
                	obtenerArticulos();
                break;
                         
                case 'false':
                	alert("Error al eliminar un registro");
                break;
            }
        }
    });
}

function limpiarCampos()
{
	/**
	 * FUncion apra limpiar los campos del modal de captura
	 *
	 */

	var clave = $("#inputClave").val("");
	var desc = $("#inputDesc").val("");
	var encargado = $("#inputEnca").val("");
	var dir = $("#cmbDir option:selected").val("");
}

/********************************************************************************************************************/

function obtenerDirecciones()
{
	/**
	 * obtener las direcciones para cargar el dropdown
	 */

	$("#cmbDir option").remove();

	 $.ajax({
	 	url:  <?php  echo "'". base_url(). "index.php/ArticulosController/obtenerDirecciones" ."'" ?>,
        cache: false,
        async: true,
        dataType: 'html',
        beforeSend: function(html){
        },
        success: function(html){
        	var rows = '<option value="0">Selecciona una opción</option>';
        	$(jQuery.parseJSON(html)).each(function(){
        		rows = rows + '<option value="'+this.id+'">'+this.nombre+'</option>';
        	});
        	$("#cmbDir").append(rows);
        }
	 });
}
</script>